package com.aait.happyholiday.Models

import java.io.Serializable

class ResendResponse:BaseResponse(),Serializable {
    val code:String?=null
}