package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.os.Handler
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.R


class SplashActivity : Parent_Activity() {
    var isSplashFinishid = false

    lateinit var logo : ImageView
    override val layoutResource: Int
        get() = R.layout.activity_splash

    override fun initializeComponents() {
//        val datePicker = PrimeDatePickerBottomSheet.newInstance(
//            HijriCalendar() ,
//            null,
//            null, // can be null
//            PickType.RANGE_START, // can be null
//            null, // can be null
//            null, // can be null
//            null, // can be null
//            null, // can be null
//            null // can be null
//        )
//
//        datePicker.setOnDateSetListener(object : PrimeDatePickerBottomSheet.OnDayPickedListener {
//
//            override fun onSingleDayPicked(singleDay: PrimeCalendar1) {
//                // TODO
//            }
//
//            override fun onRangeDaysPicked(startDay: PrimeCalendar1, endDay: PrimeCalendar1) {
//                CommonUtil.makeToast(mContext,startDay.shortDateString+" : "+endDay.shortDateString)
//            }
//
//            override fun onMultipleDaysPicked(multipleDays: List<PrimeCalendar1>) {
//                // TODO
//            }
//        })
//
//        datePicker.show(supportFragmentManager, "SOME_TAG")

        logo = findViewById(R.id.logo)


        val logoAnimation2 = AnimationUtils.loadAnimation(this, R.anim.anim_shine)

        Handler().postDelayed({
            logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (mSharedPrefManager.loginStatus==true){

                        startActivity(Intent(this@SplashActivity,MainActivity::class.java))
                        this@SplashActivity.finish()

                }else {

                    var intent = Intent(this@SplashActivity, PreLoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }, 2100)
        }, 1800)
    }




}
