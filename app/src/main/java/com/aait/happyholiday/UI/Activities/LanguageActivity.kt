package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.R


class LanguageActivity :Parent_Activity(){
    lateinit var arabic:RadioButton
    lateinit var english:RadioButton
    lateinit var menu:ImageView
    lateinit var title:TextView

    override val layoutResource: Int
        get() = R.layout.activity_app_language

    override fun initializeComponents() {


        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)

        title.visibility =View.VISIBLE

        title.text = getString(R.string.app_lang)
        menu.setOnClickListener { onBackPressed()
        finish()}
        if (mLanguagePrefManager.appLanguage.equals("ar")){
            arabic.isChecked = true
        }else{
            english.isChecked = true
        }
        arabic.setOnClickListener { mLanguagePrefManager.appLanguage = "ar"
            startActivity(Intent(this@LanguageActivity,SplashActivity::class.java))
            finish()
        }
        english.setOnClickListener { mLanguagePrefManager.appLanguage = "en"
            startActivity(Intent(this@LanguageActivity,SplashActivity::class.java))
            finish()
        }


    }
}