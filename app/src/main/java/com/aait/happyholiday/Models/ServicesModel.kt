package com.aait.happyholiday.Models

import java.io.Serializable

class ServicesModel :Serializable{
    var id:Int?=null
    var name:String?=null
    var image:String?=null
}