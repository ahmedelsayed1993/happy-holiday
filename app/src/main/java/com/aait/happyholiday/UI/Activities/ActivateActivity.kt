package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.Client
import com.aait.happyholiday.Models.ActivaiteResponse
import com.aait.happyholiday.Models.ResendResponse
import com.aait.happyholiday.Models.UserModel
import com.aait.happyholiday.Network.Service
import com.aait.happyholiday.R
import com.aait.happyholiday.Uitils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivateActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_activiate
    lateinit var code:EditText
    lateinit var confirm:Button
    lateinit var resend:TextView
    lateinit var userModel: UserModel

    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("data") as UserModel
        code = findViewById(R.id.code)
        confirm = findViewById(R.id.confirm)
        resend = findViewById(R.id.resend)

        confirm.setOnClickListener { if (CommonUtil.checkEditError(code,getString(R.string.activation_code))){
        return@setOnClickListener}else{
            check()
        }
        }
        resend.setOnClickListener { Resend() }

    }

    fun check(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.checkCode(userModel.token!!,code.text.toString())?.enqueue(object :
            Callback<ActivaiteResponse> {
            override fun onFailure(call: Call<ActivaiteResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ActivaiteResponse>,
                response: Response<ActivaiteResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@ActivateActivity,LoginActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun Resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.resend(userModel.token!!)?.enqueue(object :Callback<ResendResponse>{
            override fun onFailure(call: Call<ResendResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ResendResponse>,
                response: Response<ResendResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}