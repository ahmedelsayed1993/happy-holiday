package com.aait.happyholiday.UI.Fragments

import android.util.Log
import android.view.TextureView
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.happyholiday.Base.BaseFragment
import com.aait.happyholiday.Client
import com.aait.happyholiday.Listeners.OnItemClickListener
import com.aait.happyholiday.Models.HomeModel
import com.aait.happyholiday.Models.HomeResponse
import com.aait.happyholiday.Models.ListModel
import com.aait.happyholiday.Models.ListResponse
import com.aait.happyholiday.Network.Service
import com.aait.happyholiday.R
import com.aait.happyholiday.UI.Adapters.HomeAdapter
import com.aait.happyholiday.UI.Adapters.ListAdapter
import com.aait.happyholiday.Uitils.CommonUtil
import com.aminography.primecalendar.PrimeCalendar
import com.aminography.primecalendar.common.CalendarFactory
import com.aminography.primecalendar.common.CalendarType
import com.aminography.primecalendar.hijri.HijriCalendar
import com.aminography.primedatepicker.PickType
import com.aminography.primedatepicker.fragment.PrimeDatePickerBottomSheet
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class HomeFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (selected==0){
            listModel = listModels.get(position)
            city_id = listModel.id!!
            city.setText(listModel.name!!)
            cities.visibility=View.GONE

        }
    }

    lateinit var date:TextView
    lateinit var city:TextView
    lateinit var code:EditText
    lateinit var search: Button
    lateinit var cities:RecyclerView
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var listModels = ArrayList<ListModel>()
    internal lateinit var listAdapter: ListAdapter
    internal lateinit var listModel: ListModel
    var selected:Int = 0
    var start:String? = null
    var end:String? = null
    var city_id:Int? = null
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager1: LinearLayoutManager
    internal var homeModels = java.util.ArrayList<HomeModel>()
    internal lateinit var homeAdapter: HomeAdapter


    override val layoutResource: Int
        get() = R.layout.fragment_home

    override fun initializeComponents(view: View) {
        date = view.findViewById(R.id.date)
        city = view.findViewById(R.id.city)
        code = view.findViewById(R.id.code)
        search = view.findViewById(R.id.search)
        cities = view.findViewById(R.id.cities)
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter = mContext?.let { ListAdapter(it,listModels,R.layout.recycler_list) }!!
        listAdapter.setOnItemClickListener(this)
        cities.layoutManager = linearLayoutManager
        cities.adapter = listAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        homeAdapter = mContext?.let { HomeAdapter(it,homeModels,R.layout.recycler_home) }!!
        homeAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager1
        rv_recycle.adapter = homeAdapter
        getCities()
        city.setOnClickListener {
            selected = 0

            if (cities.visibility ==View.GONE){
                cities.visibility=View.VISIBLE
            }else{
                cities.visibility=View.GONE
            }
        }

        val calendar = CalendarFactory.newInstance(CalendarType.HIJRI)
        date.setOnClickListener {
                    val datePicker = PrimeDatePickerBottomSheet.newInstance(
            HijriCalendar() ,
            calendar,
            null, // can be null
            PickType.RANGE_START, // can be null
            null, // can be null
            null, // can be null
            null, // can be null
            null, // can be null
            null // can be null
        )


        datePicker.setOnDateSetListener(object : PrimeDatePickerBottomSheet.OnDayPickedListener {
            override fun onMultipleDaysPicked(multipleDays: List<PrimeCalendar>) {

            }

            override fun onRangeDaysPicked(startDay: PrimeCalendar, endDay: PrimeCalendar) {
                date.text = startDay.shortDateString+":"+endDay.shortDateString
                start = startDay.shortDateString
                end = endDay.shortDateString
            }

            override fun onSingleDayPicked(singleDay: PrimeCalendar) {

            }

//            override fun onSingleDayPicked(singleDay: PrimeCalendar1) {
//
//            }
//
//            override fun onRangeDaysPicked(startDay: PrimeCalendar1, endDay: PrimeCalendar1) {
//                CommonUtil.makeToast(mContext,startDay.shortDateString+" : "+endDay.shortDateString)
//            }
//
//            override fun onMultipleDaysPicked(multipleDays: List<PrimeCalendar1>) {
//
//            }
        })

        datePicker.show(childFragmentManager, "SOME_TAG")
        }
        if (mSharedPrefManager.loginStatus!!) {
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )

            swipeRefresh!!.setOnRefreshListener { estate(mSharedPrefManager.userData.token)
                city.text = getString(R.string.city)
                date.text = getString(R.string.booking_date)
                code.setText("")}
            estate(mSharedPrefManager.userData.token)
        }else{
            swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
            )

            swipeRefresh!!.setOnRefreshListener { estate(null)
            city.text = getString(R.string.city)
            date.text = getString(R.string.booking_date)
            code.setText("")}
            estate(null)
        }

        search.setOnClickListener {

            if (code.text.toString().equals("")){
                filter(null,city_id,null,start,end)
            }else {
                filter(null,city_id,code.text.toString(),start,end)
            }
        }

    }

    fun getCities(){

        Client.getClient()?.create(Service::class.java)?.Cities(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse> {

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                mContext?.let { CommonUtil.handleException(it,t) }
                t.printStackTrace()

            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {

                if (response.isSuccessful){
                    if (response.body()!!.value.equals("1")){
                        listAdapter.updateAll(response.body()!!.data!!)
                    }else{

                    }
                }
            }
        } )
    }
    fun filter(token:String?,city:Int?,code:String?,start:String?,end:String?){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getHome(mLanguagePrefManager.appLanguage,token,city,code,start,end)?.enqueue(object :Callback<HomeResponse>{
            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                mContext?.let { CommonUtil.handleException(it,t) }
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }

            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        if (response.body()?.data!!.isEmpty()){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                              homeAdapter.updateAll(response.body()?.data!!)
                        }
                    }
                }
            }

        })
    }

    fun estate(token:String?){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.estates(token,mLanguagePrefManager.appLanguage)?.enqueue(object :Callback<HomeResponse>{
            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                mContext?.let { CommonUtil.handleException(it,t) }
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }

            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        if (response.body()?.data!!.isEmpty()){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                            homeAdapter.updateAll(response.body()?.data!!)
                        }
                    }
                }
            }

        })
    }
}