package com.aait.happyholiday.Models

import java.io.Serializable

class HomeModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var title:String?=null
    var address:String?=null
    var price:String?=null
    var rate:Int?=null
    var services:ArrayList<ServicesModel>?=null
}