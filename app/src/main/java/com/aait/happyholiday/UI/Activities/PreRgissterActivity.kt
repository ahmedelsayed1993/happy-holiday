package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.widget.Button
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.R

class PreRgissterActivity :Parent_Activity(){
    lateinit var client:Button
    lateinit var provider:Button
    override val layoutResource: Int
        get() = R.layout.activity_preregister

    override fun initializeComponents() {
        client = findViewById(R.id.client)
        provider = findViewById(R.id.provider)
        client.setOnClickListener { startActivity(Intent(this@PreRgissterActivity,RegisterActivity::class.java))
        finish()}

        provider.setOnClickListener {  }

    }
}