package com.aait.happyholiday.Models

import java.io.Serializable

class HomeResponse:BaseResponse(),Serializable {
    var data:ArrayList<HomeModel>?=null
}