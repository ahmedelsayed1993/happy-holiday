package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.Client
import com.aait.happyholiday.Models.UserResponse
import com.aait.happyholiday.Network.Service
import com.aait.happyholiday.R
import com.aait.happyholiday.Uitils.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity :Parent_Activity(){
    lateinit var forgot:TextView
    lateinit var register:LinearLayout
    lateinit var phone:EditText
    lateinit var password:EditText
    lateinit var login:Button
    var deviceID=""

    override val layoutResource: Int
        get() = R.layout.activity_login

    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        login = findViewById(R.id.login)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        forgot = findViewById(R.id.forgot)
        register = findViewById(R.id.register)
        register.setOnClickListener { startActivity(Intent(this@LoginActivity,RegisterActivity::class.java)) }

        forgot.setOnClickListener { startActivity(Intent(this@LoginActivity,RecoverPasswordActivity::class.java)) }
        login.setOnClickListener { if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkEditError(password,getString(R.string.password))){
        return@setOnClickListener}else{
            login()
        }
        }

    }

    fun login(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.signIn(phone.text.toString(),password.text.toString(),deviceID,"android",mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        mSharedPrefManager.userData = response.body()?.data!!
                        mSharedPrefManager.loginStatus = true
                        startActivity(Intent(this@LoginActivity,MainActivity::class.java))
                        finish()
                    }else if (response.body()?.key.equals("3")){
                        val intent = Intent(this@LoginActivity,ActivateActivity::class.java)
                        intent.putExtra("data",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}