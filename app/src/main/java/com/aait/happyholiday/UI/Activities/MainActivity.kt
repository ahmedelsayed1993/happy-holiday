package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.Client
import com.aait.happyholiday.Models.UserResponse
import com.aait.happyholiday.Network.Service
import com.aait.happyholiday.R
import com.aait.happyholiday.UI.Fragments.HomeFragment
import com.aait.happyholiday.UI.Fragments.ProfileFragment
import com.aait.happyholiday.Uitils.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity:Parent_Activity() {

    lateinit var home:LinearLayout
    lateinit var home_image:ImageView
    lateinit var booking:LinearLayout
    lateinit var booking_image:ImageView
    lateinit var offers:LinearLayout
    lateinit var offers_image:ImageView
    lateinit var profile:LinearLayout
    lateinit var profile_image:ImageView
    lateinit var menu:ImageView
    lateinit var title:TextView
    lateinit var notification:ImageView
    lateinit var about_app:TextView
    lateinit var terms:TextView
    lateinit var contact_us:TextView
    lateinit var app_lang:TextView
    lateinit var share_app:TextView
    lateinit var main:TextView
    lateinit var image: CircleImageView
    lateinit var name:TextView
    lateinit var logout:TextView
    private var fragmentManager: FragmentManager? = null

    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var profileFragment: ProfileFragment
    override val layoutResource: Int
        get() = R.layout.activity_main

    override fun initializeComponents() {
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        booking = findViewById(R.id.booking)
        booking_image = findViewById(R.id.booking_image)
        offers = findViewById(R.id.offers)
        offers_image = findViewById(R.id.offers_image)
        profile = findViewById(R.id.profile)
        profile_image = findViewById(R.id.profile_image)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        notification = findViewById(R.id.notification)
        homeFragment = HomeFragment()
        profileFragment = ProfileFragment()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container,homeFragment)
        transaction!!.add(R.id.home_fragment_container,profileFragment)
        transaction!!.commit()
        sideMenu()
        showHome()
        home.setOnClickListener { showHome() }
        offers.setOnClickListener { showOffers() }
        booking.setOnClickListener { showBooking() }
        profile.setOnClickListener { showProfile() }


    }
    fun sideMenu(){

        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 25f)
        drawer_layout.setRadius(Gravity.END, 25f)
        drawer_layout.setViewScale(
            Gravity.START,
            0.9f
        ) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(
            Gravity.END,
            0.9f
        ) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(
            Gravity.START,
            20f
        ) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(
            Gravity.END,
            20f
        ) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewScrimColor(
            Gravity.START,
            Color.TRANSPARENT
        ) //set drawer overlay coloe (color)
        drawer_layout.setViewScrimColor(
            Gravity.END,
            Color.TRANSPARENT
        ) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 20f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 20f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(
            Gravity.START,
            40f
        ) //set end container's corner radius (dimension)
        drawer_layout.setRadius(Gravity.END, 40f)

        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }

        logout = drawer_layout.findViewById(R.id.logout)
        image = drawer_layout.findViewById(R.id.image)
        name = drawer_layout.findViewById(R.id.name)
        main = drawer_layout.findViewById(R.id.main)
        about_app = drawer_layout.findViewById(R.id.about_app)
        terms = drawer_layout.findViewById(R.id.terms)
        contact_us = drawer_layout.findViewById(R.id.contact_us)
        app_lang = drawer_layout.findViewById(R.id.app_lang)
        share_app = drawer_layout.findViewById(R.id.share)
        if (mSharedPrefManager.loginStatus!!) {
            getprofile()
        }else{
            name.visibility = View.GONE
            image.visibility = View.GONE
        }
        main.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
        about_app.setOnClickListener {

                startActivity(
                    Intent(
                        this@MainActivity,
                        AboutAppActivity::class.java
                    )
                )

        }
        terms.setOnClickListener {

                startActivity(
                    Intent(
                        this@MainActivity,
                        TermsActivity::class.java
                    )
                )

        }

        contact_us.setOnClickListener {

                startActivity(
                    Intent(
                        this@MainActivity,
                        ContactUsActivity::class.java
                    )
                )

        }
        app_lang.setOnClickListener {

                startActivity(
                    Intent(
                        this@MainActivity,
                        LanguageActivity::class.java
                    )
                )

        }
        share_app.setOnClickListener { CommonUtil.ShareApp(applicationContext) }
        logout.setOnClickListener {
//            if (mSharedPrefManager.loginStatus!!) {
//                logout()
//            }else{
//                visitor()
//            }
            }


    }

    fun getprofile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProfile(mSharedPrefManager.userData.token!!)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()

            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        name.text = response.body()?.data?.name!!
                        Glide.with(mContext).load(response.body()?.data?.avatar!!).into(image)
                    }else{

                    }
                }
            }

        })
    }
    fun showHome(){
        home_image.setImageResource(R.mipmap.homee)
        booking_image.setImageResource(R.mipmap.bookingg)
        profile_image.setImageResource(R.mipmap.profile)
        offers_image.setImageResource(R.mipmap.noun_product)
        title.text = getString(R.string.home)
        notification.visibility = View.VISIBLE
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, homeFragment)
        transaction!!.commit()
    }

    fun showOffers(){
        home_image.setImageResource(R.mipmap.home)
        booking_image.setImageResource(R.mipmap.bookingg)
        profile_image.setImageResource(R.mipmap.profile)
        offers_image.setImageResource(R.mipmap.offers)
        title.text = getString(R.string.offers)
    }
    fun showBooking(){
        home_image.setImageResource(R.mipmap.home)
        booking_image.setImageResource(R.mipmap.booking)
        profile_image.setImageResource(R.mipmap.profile)
        offers_image.setImageResource(R.mipmap.noun_product)
        title.text = getString(R.string.booking)
    }

    fun showProfile(){
        home_image.setImageResource(R.mipmap.home)
        booking_image.setImageResource(R.mipmap.bookingg)
        profile_image.setImageResource(R.mipmap.profilee)
        offers_image.setImageResource(R.mipmap.noun_product)
        title.text = getString(R.string.profile)
        notification.visibility = View.GONE
        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, profileFragment)
        transaction!!.commit()
    }
}