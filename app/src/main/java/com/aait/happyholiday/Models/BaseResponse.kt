package com.aait.happyholiday.Models

import java.io.Serializable

open class BaseResponse:Serializable {
    val key:String?=null
    val msg:String?=null
    val value:String?=null
}