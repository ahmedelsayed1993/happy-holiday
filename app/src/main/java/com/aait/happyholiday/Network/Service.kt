package com.aait.happyholiday.Network

import com.aait.happyholiday.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface Service {

    @POST("sign-up")
    fun signUp(
               @Query("name") name:String,
               @Query("phone") phone:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String):Call<UserResponse>

    @POST("check-code")
    fun checkCode(@Header("token") token:String,
                  @Query("code") code:String):Call<ActivaiteResponse>

    @POST("resend-code")
    fun resend(@Header("token") token:String):Call<ResendResponse>

    @POST("forget-password")
    fun forgotPassword(@Query("phone") phone:String):Call<UserResponse>

    @POST("update-password")
    fun updatePassword(@Header("token") token:String,
                       @Query("password") password:String,
                       @Query("lang") lang:String):Call<BaseResponse>


    @POST("sign-in")
    fun signIn(@Query("phone") phone:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("profile")
    fun getProfile(@Header("token") token:String):Call<UserResponse>

    @POST("about")
    fun About(@Query("lang") lang:String):Call<ActivaiteResponse>

    @POST("terms")
    fun getTerms(@Query("lang") lang:String):Call<ActivaiteResponse>

    @POST("contact-message")
    fun contact(@Query("lang") lang: String,
                @Query("name") name:String?,
                @Query("email") email:String?,
                @Query("phone") phone:String?,
                @Query("message") message:String?):Call<ContactUsResponse>

    @POST("cities")
    fun Cities(@Query("lang") lang:String):Call<ListResponse>

    @POST("filter")
    fun getHome(@Query("lang") lang:String,
                @Header("token") token: String?,
                @Query("city_id") city_id:Int?,
                @Query("code") code:String?,
                @Query("date_from") date_from:String?,
                @Query("date_to") date_to:String?):Call<HomeResponse>
    @POST("real-estate")
    fun estates(@Header("token") token:String?,
                @Query("lang") lang:String):Call<HomeResponse>

    @Multipart
    @POST("edit-profile")
    fun editImage(@Header("token") token:String,
                  @Query("name") name:String?,
                  @Query("phone") phone:String?,
                  @Part avatar:MultipartBody.Part):Call<UserResponse>
    @POST("edit-profile")
    fun edit(@Header("token") token:String,
                  @Query("name") name:String?,
                  @Query("phone") phone:String?):Call<UserResponse>

    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Header("token") token: String,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<BaseResponse>
}