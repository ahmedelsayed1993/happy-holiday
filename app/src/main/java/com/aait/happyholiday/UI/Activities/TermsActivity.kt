package com.aait.happyholiday.UI.Activities

import android.widget.ImageView
import android.widget.TextView
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.Client
import com.aait.happyholiday.Models.ActivaiteResponse
import com.aait.happyholiday.Network.Service
import com.aait.happyholiday.R
import com.aait.happyholiday.Uitils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TermsActivity : Parent_Activity() {
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var about: TextView
    override val layoutResource: Int
        get() = R.layout.activity_about_app

    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        about = findViewById(R.id.about)
        title.text = getString(R.string.Terms_conditions)
        menu.setOnClickListener { onBackPressed()
            finish()}
        getAbout()

    }
    fun getAbout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getTerms(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ActivaiteResponse> {
            override fun onFailure(call: Call<ActivaiteResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ActivaiteResponse>,
                response: Response<ActivaiteResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        about.text = response.body()?.data!!
                    }else{
                        about.text = getString(R.string.content_not_found_you_can_still_search_the_app_freely)
                    }
                }
            }

        })
    }
}