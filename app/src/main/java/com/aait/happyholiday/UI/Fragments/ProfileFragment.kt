package com.aait.happyholiday.UI.Fragments

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.InputType
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.aait.happyholiday.Base.BaseFragment
import com.aait.happyholiday.Client
import com.aait.happyholiday.Models.BaseResponse
import com.aait.happyholiday.Models.UserResponse
import com.aait.happyholiday.Network.Service
import com.aait.happyholiday.R
import com.aait.happyholiday.Uitils.CommonUtil
import com.aait.happyholiday.Uitils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.ArrayList

class ProfileFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_profile
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var user_name:EditText
    lateinit var phone:EditText
    lateinit var change_pass:Button
    lateinit var save:Button
    private var ImageBasePath: String? = null
    internal var returnValue: ArrayList<String>? = ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")

    override fun initializeComponents(view: View) {
        image = view.findViewById(R.id.image)
        name = view.findViewById(R.id.name)
        user_name = view.findViewById(R.id.user_name)
        phone = view.findViewById(R.id.phone)
        change_pass = view.findViewById(R.id.change_pass)
        save = view.findViewById(R.id.save)
        getprofile()
        image.setOnClickListener { getPickImageWithPermission() }
        save.setOnClickListener {
            if (CommonUtil.checkEditError(user_name,getString(R.string.user_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)){
                return@setOnClickListener
            }else{
                update()
            }
        }

        change_pass.setOnClickListener { showDialog() }

    }

    private fun showDialog() {
        val dialog = mContext?.let { Dialog(it) }
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog ?.setCancelable(true)
        dialog ?.setContentView(R.layout.dialog_change_password)
        val password = dialog?.findViewById<EditText>(R.id.password)
        val new_password = dialog?.findViewById<EditText>(R.id.new_password)
        val confirm_password = dialog?.findViewById<EditText>(R.id.confirm_new_password)

        val confirm = dialog?.findViewById<Button>(R.id.confirm)

        confirm?.setOnClickListener {
            if (CommonUtil.checkEditError(
                    password!!,
                    getString(R.string.Old_password)
                ) ||
                CommonUtil.checkEditError(
                    new_password!!,
                    getString(R.string.new_password)
                ) ||
                CommonUtil.checkLength(new_password, getString(R.string.password_length), 6) ||
                CommonUtil.checkEditError(
                    confirm_password!!,
                    getString(R.string.confirm_new_password)
                )
            ) {
                return@setOnClickListener
            } else {
                if (!confirm_password.text.toString().equals(new_password.text.toString())) {
                    confirm_password.error = getString(R.string.password_not_match)
                } else {
                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.resetPassword(
                        mLanguagePrefManager.appLanguage,
                        mSharedPrefManager.userData.token!!,
                        password?.text.toString(),
                        new_password?.text.toString()
                    )?.enqueue(object : Callback<BaseResponse> {
                        override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                            mContext?.let { CommonUtil.handleException(it, t) }
                            t.printStackTrace()
                            hideProgressDialog()
                            dialog.dismiss()

                        }

                        override fun onResponse(
                            call: Call<BaseResponse>,
                            response: Response<BaseResponse>
                        ) {
                            hideProgressDialog()
                            if (response.isSuccessful) {
                                if (response.body()?.key.equals("1")) {
                                    mContext?.let {
                                        CommonUtil.makeToast(
                                            it,
                                            response.body()?.msg!!
                                        )
                                    }
                                    dialog.dismiss()
                                } else {
                                    mContext?.let {
                                        CommonUtil.makeToast(
                                            it,
                                            response.body()?.msg!!

                                        )
                                    }
                                    dialog.dismiss()
                                }
                            }
                        }

                    })
                }
            }
        }
        dialog ?.show()


    }
    fun getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options)
        }
    }

    fun getprofile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProfile(mSharedPrefManager.userData.token!!)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                mContext?.let { CommonUtil.handleException(it,t) }
                t.printStackTrace()
                hideProgressDialog()

            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        name.text = response.body()?.data?.name!!
                        user_name.setText(response.body()?.data?.name)
                        phone.setText(response.body()?.data?.phone)
                        mContext?.let { Glide.with(it).load(response.body()?.data?.avatar!!).into(image) }
                    }else{

                    }
                }
            }

        })
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
         if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]
            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
            image!!.setImageURI(Uri.parse(ImageBasePath))
            if (ImageBasePath != null) {
                updateImage(ImageBasePath!!)
            }
        }
    }

    fun updateImage(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val file = File(path)
        val requestBody = RequestBody.create(MediaType.parse("*/*"), file)
        filePart = MultipartBody.Part.createFormData("avatar", file.name, requestBody)
        Client.getClient()?.create(Service::class.java)?.editImage(mSharedPrefManager.userData.token!!,null,null,filePart)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                mContext?.let { CommonUtil.handleException(it,t) }
                t.printStackTrace()
                hideProgressDialog()


            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        name.text = response.body()?.data?.name!!
                        user_name.setText(response.body()?.data?.name)
                        phone.setText(response.body()?.data?.phone)
                        mContext?.let { Glide.with(it).load(response.body()?.data?.avatar!!).into(image) }
                        mSharedPrefManager.userData = response.body()?.data!!
                    }else{
                        mContext?.let { CommonUtil.makeToast(it,response.body()?.msg!!) }
                    }
                }
            }

        })
    }
    fun update(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.edit(mSharedPrefManager.userData.token!!,user_name.text.toString(),phone.text.toString())?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                mContext?.let { CommonUtil.handleException(it,t) }
                t.printStackTrace()
                hideProgressDialog()


            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        name.text = response.body()?.data?.name!!
                        user_name.setText(response.body()?.data?.name)
                        phone.setText(response.body()?.data?.phone)
                        mContext?.let { Glide.with(it).load(response.body()?.data?.avatar!!).into(image) }
                        mSharedPrefManager.userData = response.body()?.data!!
                    }else{
                        mContext?.let { CommonUtil.makeToast(it,response.body()?.msg!!) }
                    }
                }
            }

        })

    }
}