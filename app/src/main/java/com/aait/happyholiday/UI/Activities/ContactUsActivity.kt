package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.Client
import com.aait.happyholiday.Models.ContactUsResponse
import com.aait.happyholiday.Network.Service
import com.aait.happyholiday.R
import com.aait.happyholiday.Uitils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUsActivity :Parent_Activity(){
    lateinit var menu:ImageView
    lateinit var title:TextView
    lateinit var full_name:EditText
    lateinit var email:EditText
    lateinit var phone:EditText
    lateinit var message:EditText
    lateinit var send:Button
    lateinit var insta:ImageView
    lateinit var snap:ImageView
    lateinit var twitter:ImageView
    var instagram = ""
    var snapchat = ""
    var twitt = ""
    override val layoutResource: Int
        get() = R.layout.activity_contact_us

    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        full_name = findViewById(R.id.full_name)
        email = findViewById(R.id.email)
        phone = findViewById(R.id.phone)
        message = findViewById(R.id.message)
        send = findViewById(R.id.send)
        insta = findViewById(R.id.insta)
        snap = findViewById(R.id.snap)
        twitter = findViewById(R.id.twitter)
        menu.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.contact_us)
        contact()
        insta.setOnClickListener {
            if (!instagram.equals("")) {
                if (instagram.startsWith("http")) {
                    Log.e("here", "333")
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(instagram)
                    startActivity(i)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$instagram"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }
        twitter.setOnClickListener {
            if (!twitt.equals("")) {
                if (twitt.startsWith("http")) {
                    Log.e("here", "333")
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(twitt)
                    startActivity(i)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$twitt"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }
        snap.setOnClickListener {
            if (!snapchat.equals("")) {
                if (snapchat.startsWith("http")) {
                    Log.e("here", "333")
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(snapchat)
                    startActivity(i)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$snapchat"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }

        send.setOnClickListener {
            if (CommonUtil.checkEditError(full_name,getString(R.string.full_name))||
                    CommonUtil.checkEditError(email,getString(R.string.email))||
                    CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkEditError(message,getString(R.string.message))){
                return@setOnClickListener
            }else{
                ContactUs()
            }
        }

    }

    fun contact(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.contact(mLanguagePrefManager.appLanguage,null,null,null,null)?.enqueue(object :
            Callback<ContactUsResponse> {
            override fun onFailure(call: Call<ContactUsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ContactUsResponse>,
                response: Response<ContactUsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        instagram = response.body()?.social?.instagram!!
                        twitt = response.body()?.social?.twitter!!
                        snapchat = response.body()?.social?.snapchat!!


                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun ContactUs(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.contact(mLanguagePrefManager.appLanguage,full_name.text.toString()
            ,email.text.toString(),phone.text.toString(),message.text.toString())?.enqueue(object :
            Callback<ContactUsResponse> {
            override fun onFailure(call: Call<ContactUsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ContactUsResponse>,
                response: Response<ContactUsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        instagram = response.body()?.social?.instagram!!
                        twitt = response.body()?.social?.twitter!!
                        snapchat = response.body()?.social?.snapchat!!
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)


                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}