package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.TextView
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.R

class PreLoginActivity:Parent_Activity() {
    lateinit var login:Button
    lateinit var register:Button
    lateinit var browse:TextView
    override val layoutResource: Int
        get() = R.layout.activity_prelogin

    override fun initializeComponents() {
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)
        browse = findViewById(R.id.browse)

        login.setOnClickListener { startActivity(Intent(this@PreLoginActivity,LoginActivity::class.java))
        finish()}
        register.setOnClickListener { startActivity(Intent(this@PreLoginActivity,PreRgissterActivity::class.java))
        finish()}
        browse.setOnClickListener {  }

    }
}