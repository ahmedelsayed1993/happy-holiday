package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.Models.UserResponse
import com.aait.happyholiday.Network.Service
import com.aait.happyholiday.R
import com.aait.happyholiday.Uitils.CommonUtil
import com.aait.happyholiday.Client
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity :Parent_Activity(){
    lateinit var sign_up:Button
    lateinit var user_name:EditText
    lateinit var phone:EditText
    lateinit var password:EditText
    lateinit var confirm_password:EditText
    lateinit var login:LinearLayout

    var deviceID=""
    override val layoutResource: Int
        get() = R.layout.activity_register

    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        login = findViewById(R.id.login)
        user_name = findViewById(R.id.user_name)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        confirm_password = findViewById(R.id.confirm_password)
        sign_up = findViewById(R.id.sign_up)
        login.setOnClickListener { startActivity(Intent(this@RegisterActivity,LoginActivity::class.java))
        finish()}
        sign_up.setOnClickListener { if (CommonUtil.checkEditError(user_name,getString(R.string.user_name))||
                CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)) {
            return@setOnClickListener
        }else{
            if (!password.text.toString().equals(confirm_password.text.toString())){
                CommonUtil.makeToast(mContext,getString(R.string.password_not_match))
            }else{
                signUp()
            }
        }
        }

    }

    fun signUp(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.signUp(user_name.text.toString()
        ,phone.text.toString(),password.text.toString(),deviceID,"android")?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        val intent = Intent(this@RegisterActivity,ActivateActivity::class.java)
                        intent.putExtra("data",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}