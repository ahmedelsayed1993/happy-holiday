package com.aait.happyholiday.Models

import java.io.Serializable

class ContactUsResponse:BaseResponse(),Serializable {
    var social:SocialModel?=null
}