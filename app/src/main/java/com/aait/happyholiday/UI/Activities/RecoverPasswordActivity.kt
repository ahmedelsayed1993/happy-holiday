package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.Client
import com.aait.happyholiday.Models.UserResponse
import com.aait.happyholiday.Network.Service
import com.aait.happyholiday.R
import com.aait.happyholiday.Uitils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecoverPasswordActivity :Parent_Activity(){
    lateinit var confirm:Button
    lateinit var phone:EditText
    override val layoutResource: Int
        get() = R.layout.activity_recover_password

    override fun initializeComponents() {
        phone = findViewById(R.id.phone)
        confirm = findViewById(R.id.confirm)
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))){
                return@setOnClickListener
            }else{
                forgot()
            }
        }

    }
    fun forgot(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.forgotPassword(phone.text.toString())?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        val intent = Intent(this@RecoverPasswordActivity,NewPasswordActivity::class.java)
                        intent.putExtra("data",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}