package com.aait.happyholiday.Models

import java.io.Serializable

class SocialModel:Serializable {
    var twitter:String?=null
    var instagram:String?=null
    var snapchat:String?=null
}