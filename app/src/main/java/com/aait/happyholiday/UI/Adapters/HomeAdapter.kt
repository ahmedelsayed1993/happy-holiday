package com.aait.happyholiday.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.happyholiday.Base.ParentRecyclerAdapter
import com.aait.happyholiday.Base.ParentRecyclerViewHolder
import com.aait.happyholiday.Models.HomeModel
import com.aait.happyholiday.Models.ListModel
import com.aait.happyholiday.R
import com.bumptech.glide.Glide

class HomeAdapter (context: Context, data: MutableList<HomeModel>, layoutId: Int) :
    ParentRecyclerAdapter<HomeModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val homeModel = data.get(position)
        viewHolder.name!!.setText(homeModel.title)
        viewHolder.address.text = homeModel.address
        viewHolder.price.text = homeModel.price+mcontext.getString(R.string.Rs)
        viewHolder.rating.rating = homeModel.rate?.toFloat()!!
        Glide.with(mcontext).load(homeModel.image).into(viewHolder.image)
        if (homeModel.services?.isEmpty()!!){
            viewHolder.image1.visibility = View.GONE
            viewHolder.image2.visibility = View.GONE
            viewHolder.image3.visibility = View.GONE
        }else{
            if (homeModel.services?.size==3){
                viewHolder.image1.visibility = View.VISIBLE
                viewHolder.image2.visibility = View.VISIBLE
                viewHolder.image3.visibility = View.VISIBLE
                Glide.with(mcontext).load(homeModel.services?.get(0)?.image).into(viewHolder.image1)
                Glide.with(mcontext).load(homeModel.services?.get(1)?.image).into(viewHolder.image2)
                Glide.with(mcontext).load(homeModel.services?.get(2)?.image).into(viewHolder.image3)
            }
            if (homeModel.services?.size==2){
                viewHolder.image1.visibility = View.VISIBLE
                viewHolder.image2.visibility = View.VISIBLE
                viewHolder.image3.visibility = View.GONE
                Glide.with(mcontext).load(homeModel.services?.get(0)?.image).into(viewHolder.image1)
                Glide.with(mcontext).load(homeModel.services?.get(1)?.image).into(viewHolder.image2)

            }
            if (homeModel.services?.size==1){
                viewHolder.image1.visibility = View.VISIBLE
                viewHolder.image2.visibility = View.GONE
                viewHolder.image3.visibility = View.GONE
                Glide.with(mcontext).load(homeModel.services?.get(0)?.image).into(viewHolder.image1)

            }
        }
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var image1 = itemView.findViewById<ImageView>(R.id.image1)
        internal var image2 = itemView.findViewById<ImageView>(R.id.image2)
        internal var image3 = itemView.findViewById<ImageView>(R.id.image3)
        internal var rating = itemView.findViewById<RatingBar>(R.id.rating)
        internal var price = itemView.findViewById<TextView>(R.id.price)
        internal var address = itemView.findViewById<TextView>(R.id.address)


    }
}