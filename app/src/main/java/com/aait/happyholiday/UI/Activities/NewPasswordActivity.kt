package com.aait.happyholiday.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import com.aait.happyholiday.Base.Parent_Activity
import com.aait.happyholiday.Client
import com.aait.happyholiday.Models.BaseResponse
import com.aait.happyholiday.Models.UserModel
import com.aait.happyholiday.Network.Service
import com.aait.happyholiday.R
import com.aait.happyholiday.Uitils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewPasswordActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_new_password

    lateinit var new_password:EditText
    lateinit var confirm_password:EditText
    lateinit var confirm:Button
    lateinit var userModel: UserModel

    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("data") as UserModel
        new_password = findViewById(R.id.new_password)
        confirm_password = findViewById(R.id.confirm_password)
        confirm = findViewById(R.id.confirm)
        confirm.setOnClickListener { if (CommonUtil.checkEditError(new_password,getString(R.string.new_password))||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_new_password))||
                CommonUtil.checkLength(new_password,getString(R.string.password_length),6)
                ){
        return@setOnClickListener}else{
            if (!new_password.text.toString().equals(confirm_password.text.toString())){
                CommonUtil.makeToast(mContext,getString(R.string.password_not_match))
            }else{
                newPassword()

            }
        }
        }

    }
    fun newPassword(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.updatePassword(userModel.token!!,new_password.text.toString(),mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<BaseResponse>{
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.key.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@NewPasswordActivity,LoginActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}